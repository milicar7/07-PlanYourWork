const mongoose = require('mongoose');

const projectSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    team: {
        type: [String],
        required : true
    },
    boards: {
        type: [mongoose.Schema.Types.ObjectId], 
        required: false,
        default: []
    }

}, {collection : "projects"});

const projectModel = mongoose.model('project', projectSchema);

module.exports = projectModel;