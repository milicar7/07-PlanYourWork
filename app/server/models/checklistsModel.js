const mongoose = require('mongoose');

const checklistsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {
        type: String,
        required: true
    },
    hideItems: {
        type: Boolean,
        default: false
    },
    hideItemsText: {
        type: String
    },
    progressPercentage:{
        type: Number,
        default: 0
    },
    items: {
        type: [{name : String, checked: Boolean}],
        default: [],
        unique : true
    }
});

const checklistsModel = mongoose.model('checklists', checklistsSchema);

module.exports = checklistsModel;