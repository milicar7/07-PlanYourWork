require('dotenv').config()
const JWT = require('jsonwebtoken')

module.exports = {
  signAccessToken: (user,access) => {
    return new Promise((resolve, reject) => {
      const payload = {
        "username": user.username,
        "fullname": user.fullname,
        "email":user.email,
        "projects":user.projects,
        "imgUrl": user.imgUrl
      }
      let secret;
      let expire;
      if(access === true){  
         secret =   "" + process.env.ACCESS_TOKEN_SECRET;
        expire = '60m';
         
    }else{
      secret =   "" + process.env.REFRESH_TOKEN_SECRET;
   expire = '30d';
    }
    const options = {
        expiresIn: expire,
        issuer: 'planyourwork',
        audience: [user._id],
      }
      JWT.sign(payload, secret, options, (err, token) => {
        if (err) {
          console.log(err.message);
          reject({message: "Internal server error!"});
          return

        }
        if(!access){
     
        }
        resolve(token)
      })
    })
  },
  
  verifyAccessToken: (req, res, next) => {
    if (!req.headers['authorization']) {
      res.status(500).send({message:"Missing auth header!"});
      return;
    }
    const authHeader = req.headers['authorization']
    const bearerToken = authHeader.split(' ')
    const token = bearerToken[1] 
    JWT.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, payload) => {
      if (err) {
        const message =  err.name === 'JsonWebTokenError' ? 'Unauthorized' : err.message
        return next({message:message})
      }
      loggedInUser= payload.username;
      req.body.loggedInUser = loggedInUser;
    });
  next() 
  }, 
  verifyRefreshToken: (refreshToken) => {
    return new Promise((resolve, reject) => {
      JWT.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, payload) => {
          if (err) {
            return reject({message: "refresh invalid!"})
          }
          const user = {
            _id: payload.aud,
            username : payload.username,
            fullname : payload.fullname,
            email : payload.email,
            projects : payload.projects
          };
          return resolve(user)
        }
      )
    })
  }
}