const express = require('express');
const router = express.Router();

const controller = require('../../controllers/usersController');
const projectController = require('../../controllers/projectController');
const jwt_helper = require('../../helpers/jwt_helper');
// http://localhost:5000/api/users
router.get('/', controller.getUsers);

// http://localhost:5000/api/users/:username
router.get('/:username',jwt_helper.verifyAccessToken,controller.getUserByUsername);

// http://localhost:5000/api/users/refresh_token
router.post('/refresh_token', controller.refreshToken)
// http://localhost:5000/api/users/logout
router.post('/logout', jwt_helper.verifyAccessToken, controller.logoutUser)
// http://localhost:5000/api/users/username/projects
router.get('/:username/projects',jwt_helper.verifyAccessToken, controller.getProjectsOfTheUser);
// http://localhost:5000/api/users/register
router.post('/register', controller.createUser,controller.loginUser);
// http://localhost:5000/api/users/login
router.post('/login', controller.loginUser);
// http://localhost:5000/api/users/:username
router.put('/:id',  jwt_helper.verifyAccessToken, controller.updateUserByUsername);
// http://localhost:5000/api/users/username
router.delete('/:username',  jwt_helper.verifyAccessToken, controller.deleteUserByUsername,  projectController.deleteUserFromAllProjects);

// http://localhost:5000/api/users/team/:project_id
router.put('/team/:project_id', jwt_helper.verifyAccessToken, controller.removeProjectForMultipleUsers);

// http://localhost:5000/api/users/id/project_id
router.delete('/:username/:project_id', jwt_helper.verifyAccessToken, controller.leaveProject);

router.patch('/:username/profile-image', controller.changeUserProfileImage);
// http://localhost:5000/api/users/usersInfo
router.post('/usersInfo', controller.fetchUsersInfo)

router.use((req, res, next) => {
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json({'message': `Unknown request ${unknownMethod} to ${unknownPath}`});
});
module.exports = router;