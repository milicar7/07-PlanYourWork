const express = require('express');
const router = express.Router();
const jwt_helper = require('../../helpers/jwt_helper');
const controller = require('../../controllers/boardController');
const listController = require('../../controllers/listsController');

// http://localhost:5000/api/board
router.get('/', controller.getBoards);

// http://localhost:5000/api/board/boardId
router.get('/:boardId', jwt_helper.verifyAccessToken , controller.getBoardById)

// http://localhost:5000/api/board
router.post('/', jwt_helper.verifyAccessToken , controller.createBoard);

// http://localhost:5000/api/board/boardId
router.put('/:boardId',jwt_helper.verifyAccessToken,  controller.updateBoardById);

// http://localhost:5000/api/board/boardId
router.delete('/:boardId', jwt_helper.verifyAccessToken, controller.deleteBoardById);

router.use((req, res, next) => {
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json({'message': `Unknown request ${unknownMethod} to ${unknownPath}`});
});

module.exports = router;