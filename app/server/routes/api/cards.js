const express = require('express');
const router = express.Router();

const jwt_helper = require('../../helpers/jwt_helper');
const controller =  require('../../controllers/cardsController');
const projectCtrl =  require('../../controllers/projectController');
const checklistCtrl =  require('../../controllers/checklistsController');
const listCtrl =  require('../../controllers/listsController');
// http://localhost:5000/api/cards
router.get('/', controller.getCards);

// http://localhost:5000/api/cards/objectId
router.get('/:id', jwt_helper.verifyAccessToken, controller.getCardById)

// http://localhost:5000/api/cards
router.post('/',jwt_helper.verifyAccessToken, controller.createCard);

// http://localhost:5000/api/cards/upload/objectId
router.put('/upload/:id', jwt_helper.verifyAccessToken, controller.putCardFile);

// http://localhost:5000/api/cards/objectId
router.patch('/:id', jwt_helper.verifyAccessToken, controller.updateCardById);

// http://localhost:5000/api/cards/:listId
router.delete('/:listId/:card_id',jwt_helper.verifyAccessToken, listCtrl.removeCardFromList, controller.deleteCardById);

// http://localhost:5000/api/cards/objectId/delete_checklist
router.post('/:id/delete_checklist', controller.deleteChecklist, checklistCtrl.deleteChecklistById);


router.use((req, res, next) => {
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json({'message': `Unknown request ${unknownMethod} to ${unknownPath}`});
});
module.exports = router;