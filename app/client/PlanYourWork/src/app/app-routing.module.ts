import { HomePageComponent } from './home-page/home-page.component';
import { BoardComponent } from './board/board.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent} from './login/login.component';
import { RegisterComponent} from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from './guards/auth.guard';
import { CardComponent } from './card/card.component';
import { BoardGuard } from './guards/board.guard';

const routes: Routes = [
  { path: '', pathMatch: 'full', component:HomePageComponent, canActivate:[AuthGuard]},
  { path: '', component: HomePageComponent },
  { path: 'project/:projectId/:boardId', component: BoardComponent,canActivate:[BoardGuard]},
  { path: 'cards/:cardId', component: CardComponent},
  { path: 'login', component: LoginComponent,canActivate:[AuthGuard]},
  { path: 'register', component: RegisterComponent,canActivate:[AuthGuard] },
  { path: 'profile/:username', component: ProfileComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
