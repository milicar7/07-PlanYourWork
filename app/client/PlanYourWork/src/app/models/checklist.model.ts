export interface Checklist {
  _id: string;
  title: string;
  hideItems: boolean;
  hideItemsText: string;
  progressPercentage: number;
  items: [{ name: string, checked: boolean}];
}
