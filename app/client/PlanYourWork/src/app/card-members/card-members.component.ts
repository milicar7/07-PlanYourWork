import { ProfileService } from './../services/profile.service';
import {Subscription} from "rxjs";
import { Component, OnInit, Input, Output, EventEmitter, ViewChild,
          ElementRef, AfterViewChecked, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-card-members',
  templateUrl: './card-members.component.html',
  styleUrls: ['./card-members.component.css']
})
export class CardMembersComponent implements OnInit, AfterViewChecked, OnDestroy {

  @Input()
  boardMembers: string[];

  activesubs : Subscription[] = [];
  searchText: string = '';

  @Input()
  addedMembers: string[];

  @Input()
  projId: string;

  @Output()
  emitShowMembers: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  emitNewMember: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('searchMembers')
  private searchMembers: ElementRef;

  constructor(private profileService: ProfileService) {}

  ngOnInit(): void {}

  ngOnDestroy() : void{
    this.activesubs.forEach((sub) => sub.unsubscribe());
    this.activesubs = [];
  }

  ngAfterViewChecked(): void {
    this.searchMembers.nativeElement.focus();
  }

  onCloseMembers(): void{ this.emitShowMembers.emit(false);}

  onAddMember(button: HTMLElement): void{
    const buttonId = button.id
    const member: string[] = this.boardMembers.filter(member => member === buttonId);
    this.emitNewMember.emit(member[0]);
  }

  memberFound(chosenMember: string): boolean{
    const found = this.addedMembers.filter(member => member === chosenMember) ;
    if(found.length > 0){
      return true;
    }
    else{
      return false;
    }
  }
}
