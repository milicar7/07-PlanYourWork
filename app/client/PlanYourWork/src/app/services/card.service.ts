import { MessageService } from './message.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Card } from '../models/card.model';
import { Checklist } from '../models/checklist.model';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  public readonly changeCardSubject: Subject<Object> = new Subject<Object>();
  public card: Observable<Object> = this.changeCardSubject.asObservable();

  public cards: Observable<Card[]>;
  public showCard: boolean = false;
  private readonly cardsUrl = 'http://localhost:5000/api/cards/'; //na ovu adresu se salju zahtevi
  private readonly cardAttachmentUrl = 'http://localhost:5000/api/cards/upload/';

  constructor(private http: HttpClient,
              private messageService:MessageService) {
    this.refreshCards();
  }

  sendMessage(which : String, update: boolean, text = "") : void{
    this.messageService.sendMessage(which, update, text);
  }
  clearMessages( msg : String) : void{
    this.messageService.clearMessages();
  }


  public refreshCards(): Observable<Card[]>{
    this.cards = this.http.get<Card[]>(this.cardsUrl);
    return this.cards;
  }

  public getCards(): Observable<Card[]>{
    return this.cards;
  }

  public createNewCard(name: string, listId: string): Observable<Card>{
    let body = {
      "title": name,
      "listId": listId
    };

    return this.http.post<Card>(this.cardsUrl, body)
                    .pipe(tap(() => {return this.sendMessage("card", true);}));;
  }
  public getCardById(id: string): Observable<Card> {
    return this.http
      .get<Card>(this.cardsUrl + id)
  }

  public changeCardTitle(cardId: string, cardTitle: string): Observable<Object> {
    const body = {
      title: cardTitle
    };

    return this.http.patch(this.cardsUrl + cardId, body);

  }

  public changeCardDescription(cardId: string, cardDescription: string): Observable<Object> {
    const body = {
      description: cardDescription
    };

    return this.http.patch(this.cardsUrl + cardId, body);

  }

  public changeCardMembers(cardId: string, cardMembers: string[]): Observable<Object> {
    const body = {
      members: cardMembers
    };

    return this.http.patch(this.cardsUrl + cardId, body);

  }

  public changeCardLabels(cardId: string, cardLabels: string[]): Observable<Object> {
    const body = {
      labels: cardLabels
    };

    return this.http.patch(this.cardsUrl + cardId, body);

  }

  public changeCardChecklists(cardId: string, cardChecklist: Checklist): Observable<Object> {
    const body = {
      checklist: cardChecklist
    };

    return this.http.patch(this.cardsUrl + cardId, body);

  }

  public changeCardDueDate(cardId: string, cardDueDate: Date, cardCompletionStatus: string): Observable<Object> {
    const body = {
      dueDate: cardDueDate,
      completionStatus: cardCompletionStatus
    };

    return this.http.patch(this.cardsUrl + cardId, body);

  }

  public removeCardDueDate(cardId: string, remove: boolean, cardCompletionStatus: string): Observable<Object> {
    const body = {
      removeDueDate: remove,
      completionStatus: cardCompletionStatus
    };

    return this.http.patch(this.cardsUrl + cardId, body);

  }

  public putCardAttachment(cardId: string, file: File): Observable<HttpEvent<FormData>> {
    const formData: FormData = new FormData();
    formData.append("file", file);

    const headers: HttpHeaders = new HttpHeaders();

    const req: HttpRequest<FormData> = new HttpRequest<FormData>(
      "PUT",
      this.cardAttachmentUrl + cardId,
      formData,
      {
        headers,
        reportProgress: true
      }
    );
    return this.http.request<FormData>(req);
  }

  public deleteCardAttachment(cardId: string, index: number): Observable<Object>{
    const body = {
      fileIndex:  index
    };

    return this.http.patch(this.cardsUrl + cardId, body);
  }

  public changeHideAttachments(cardId: string): Observable<Object>{
    const body = {
      updateAttachmentVisability: true
    };

    return this.http.patch(this.cardsUrl + cardId, body);
  }


  public changeCardCover(cardId: string, cardCover: string): Observable<Object> {
    const body = {
      cover: cardCover
    };

    return this.http.patch(this.cardsUrl + cardId, body);
  }

  public changeListIdOfCard(cardId: string, listId: string): Observable<Object> {
    const body = {
      listId: listId
    };

    return this.http.patch(this.cardsUrl + cardId, body);
  }

  public deleteChecklist(cardId: string, checklistId : string): Observable<Object> {
    const body = {
      checklistId: checklistId
    };

    return this.http.post(this.cardsUrl + cardId + "/delete_checklist", body)
                    .pipe(tap(() => {return this.sendMessage("card", true);}));

  }
  public deleteCard(card_id:string,listId:string): Observable<Object> {
    return this.http.delete(this.cardsUrl+listId + "/" + card_id);
  }


}
