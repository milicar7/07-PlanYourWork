import { ProfileService } from './../services/profile.service';
import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public regForm: FormGroup;
  private activeSubscriptions : Array<Subscription> = [];
  fieldTextType: boolean;

  constructor(
    private formBuilder:FormBuilder,
    private profileService: ProfileService,
    private authService:AuthService,
    private router:Router,
    private toastrService:ToastrService) {

    this.regForm = this.formBuilder.group({
      email:['',[Validators.required,Validators.email]],
      username:['',[Validators.required]],
      name:['',[Validators.required]],
      password:['',[Validators.required,Validators.minLength(6),Validators.pattern('(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*')]],
      confirm:['',[Validators.required,Validators.minLength(6), Validators.pattern('(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*')]]


    });
   }

  ngOnInit(): void {}

  public get email(){ return this.regForm.get('email');}

  public get username(){ return this.regForm.get('username');}

  public get name(){ return this.regForm.get('name');}

  public get password(){ return this.regForm.get("password");}

  public get confirm(){ return this.regForm.get('confirm');}


  toggleFieldTextType() { this.fieldTextType = !this.fieldTextType;}

  public submitForm(){

    if (this.password.value !== this.confirm.value) {
      this.toastrService.error("Passwords don't match!", 'Notification');
      return;
    }

    const data = {
      "email":this.email.value,
      "username":this.username.value,
      "password":this.password.value,
      "fullname":this.name.value
     };
  const registerSub =  this.authService.createAUser(data)
    .subscribe((tokens:any)=>{
      const fetchedUser = this.authService.getUserFromToken(tokens.accessToken,tokens.refreshToken)
      this.router.navigate(['/profile',fetchedUser.username]);
    },
    (error) => {
      this.toastrService.error(error.error.message, 'Notification');
    }
    );
    this.activeSubscriptions.push(registerSub);

  }
  ngOnDestroy() { this.activeSubscriptions.forEach((sub:Subscription) => {sub.unsubscribe()});}

}
