import { Subscription } from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';
import { ChecklistService } from '../services/checklist.service';
import { CardService } from '../services/card.service';
import { Checklist } from '../models/checklist.model';
import { Card } from '../models/card.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.css']
})

export class ChecklistComponent implements OnInit{

  @Input()
  checklist: Checklist;

  @Input()
  card: Card;

  private activeSubscriptions: Subscription[];

  progressPercentage: number;
  showProgressPercentage: string;

  items: checklistItem[];
  askForItemName: boolean;
  itemName: string;

  hideCheckedItems: boolean;
  hideOrShow: string;

  constructor(private checklistService: ChecklistService,
              private cardService: CardService,
              private toastrService:ToastrService) {
    this.progressPercentage = 0;
    this.showProgressPercentage = '0';
    this.itemName = '';
    this.askForItemName = true;
    this.activeSubscriptions = [];
  }

  ngOnInit(): void {
    this.items = this.checklist.items.map((item) => {return {name:item.name, checked:item.checked, id:''}});
    this.changeProgress();
    this.hideOrShow = this.checklist.hideItemsText ? this.checklist.hideItemsText : "Hide checked items";
    this.hideCheckedItems = this.checklist.hideItems;

  }

  onChangeClTitle(newTitle: string): void{
    if (this.checklist.title === newTitle){
      return;
    }
    this.checklist.title = newTitle;
    const updateTitleSub = this.checklistService
                               .changeChecklistTitle(this.checklist._id, this.checklist.title)
                               .subscribe();
    this.activeSubscriptions.push(updateTitleSub);
  }

  onDeleteChecklist(): void{
    const sub = this.cardService.deleteChecklist(this.card._id, this.checklist._id)
                    .subscribe();
    this.activeSubscriptions.push(sub);
  }

  onAddItem(): void{ this.askForItemName = true;}

  onAddCheckbox(): void{
    if (this.itemName !== ''){
      const sub = this.checklistService.addChecklistItem(this.checklist._id, this.itemName.trim())
                      .pipe()
                    .subscribe((item : any) =>
                    {
                      this.items.push({name:item.name, checked:false, id:''});
                      this.changeProgress();
                      this.itemName = '';
                    },
                    (error) => {
                      this.toastrService.error(error.error.message, 'Error');

                    });
      this.activeSubscriptions.push(sub);

    }

  }

  changeProgress(): void{
    const numChecked = this.items.filter(item => item.checked === true) ;
    if(this.items.length === 0){
      this.progressPercentage = 0;
    }else{
      this.progressPercentage = numChecked.length * 100 / (this.items.length);
    }
    this.showProgressPercentage = this.progressPercentage.toFixed(0);

  }

  getProgressType(): string{
    let type: string = 'secondary';
    if (this.progressPercentage === 100){
      type = 'success';
    }
    return type;
  }

  onCloseItemInput(): void{
    this.askForItemName = false;
    this.itemName = '';
  }

  deleteItem(chosenId: string, name: string): void{
    const itemIndex: number = this.items.findIndex((item) => item.id === chosenId);

    if (itemIndex === -1) {
      return;
    }
    const sub = this.checklistService.deleteItem(this.checklist._id, name)
                    .subscribe(() => {
                      this.items.splice(itemIndex, 1);
                      this.changeProgress();
                    });
    this.activeSubscriptions.push(sub);
  }

  foundChecked(): boolean{
    const numChecked = this.items.filter(item => item.checked === true);
    if(numChecked.length > 0){
      return true;
    }else{
      return false;
    }
  }

  calculateStyleDelete() {
    let leftMarginSizeChoice: string;
    const numChecked = this.items.filter(item => item.checked === true);
    if (numChecked.length > 0){
      if (this.hideOrShow.substr(0, 1) === 'S'){
        leftMarginSizeChoice = '0.7rem';
      }else{
        leftMarginSizeChoice = '2.65rem';
      }
    }else{
      leftMarginSizeChoice = '13rem';
    }

    return {
        'margin-left': leftMarginSizeChoice
    };
  }

  onHideCheckedItems(): void{
    if(this.hideCheckedItems){
      this.hideCheckedItems = false;
      this.hideOrShow = 'Hide checked items';
    }else{
      this.hideCheckedItems = true;
      const numChecked = this.items.filter(item => item.checked === true) ;
      this.hideOrShow = 'Show checked items (' + numChecked.length.toString() + ')';
    }
    const sub = this.checklistService.hideItems(this.checklist._id, this.hideCheckedItems, this.hideOrShow)
                    .subscribe();
    this.activeSubscriptions.push(sub);
  }

  updateHiddenItemsCount(): void{
    if(this.hideCheckedItems){
      const numChecked = this.items.filter(item => item.checked === true) ;
      this.hideOrShow = 'Show checked items (' + numChecked.length.toString() + ')';
    }
  }
}

export type checklistItem = {
  id: string;
  name: string;
  checked: boolean;
};
