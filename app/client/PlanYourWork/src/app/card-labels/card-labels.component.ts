import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-card-labels',
  templateUrl: './card-labels.component.html',
  styleUrls: ['./card-labels.component.css']
})

export class CardLabelsComponent implements OnInit {

  @Input()
  addedLabels: string[];

  @Output()
  emitShowLabels: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  emitLabelChosen: EventEmitter<string> = new EventEmitter<string>();

  public labelColors: string[];

  constructor() { this.labelColors = ['green', 'yellow', 'orange', 'red', 'purple', 'blue'];}

  ngOnInit(): void {}

  onCloseLabels(): void{ this.emitShowLabels.emit(false);}

  onLabelChosen(colorIndex: number): void{ this.emitLabelChosen.emit(this.labelColors[colorIndex]);}

  labelFound(chosenLabel: string): boolean{
    const found = this.addedLabels.filter((label) => label === chosenLabel) ;
    if(found.length > 0){
      return true;
    }
    else{
      return false;
    }
  }
}
